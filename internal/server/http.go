package server

import (
	"gitlab.com/domano/memestream"
	"html/template"
	"io"
	"log"
	"net/http"
)

func NewTemplateRenderer(mapper Mapper) *TemplateRenderer {
	return &TemplateRenderer{mapper}
}

type TemplateRenderer struct {
	mapper Mapper
}

func (t *TemplateRenderer) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Set("Content-Type", "text/html")
	render(t.mapper, rw)
}

func render(mapper Mapper, w io.Writer) {
	tpl, err := template.New("memestream").Parse(`
<html>
<body>
{{ range $key, $value := . }}
   <strong>{{ $key }}</strong><br>
<img src="{{ $value.URL }}"/><br>
{{ end }}
</body>
</html>
`)
	if err != nil {
		log.Fatal(err)
	}

	postMap, err := mapper.AsMap()
	if err != nil {
		log.Fatal(err)
	}
	err = tpl.Execute(w, postMap)
	if err != nil {
		log.Fatal(err)
	}
}

type Mapper interface {
	AsMap() (map[string]memestream.Post, error)
}

package db

import (
	"encoding/json"
	"fmt"
	"gitlab.com/domano/memestream"
	"log"
	"net/url"
	"time"

	bolt "go.etcd.io/bbolt"
)

const postBucket = "posts"
const dedupeBucket = "filenames"

type BoltDB struct {
	conn *bolt.DB
}

func NewBolt() *BoltDB {
	// Open the my.db data file in your current directory.
	// It will be created if it doesn't exist.
	db, err := bolt.Open("my.db", 0600, &bolt.Options{Timeout: time.Second})
	if err != nil {
		log.Fatal(err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(postBucket))
		return err
	})
	if err != nil {
		log.Fatal(err)
	}
	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(dedupeBucket))
		return err
	})
	if err != nil {
		log.Fatal(err)
	}
	return &BoltDB{db}
}

func (b *BoltDB) Get(k string) (post memestream.Post, ok bool, err error) {
	err = b.conn.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(postBucket))
		maybeVal := bucket.Get([]byte(k))
		if maybeVal == nil {
			ok = false
			return nil
		}
		err := json.Unmarshal(maybeVal, &post)
		if err != nil {
			return err
		}
		ok = true
		return nil
	})
	return
}

func (b *BoltDB) Add(k string, v memestream.Post) error {
	return b.conn.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(postBucket))
		postJson, err := json.Marshal(v)
		if err != nil {
			return err
		}
		if exists, err := CheckIfMemeExists(tx, v); exists || err != nil {
			return err
		}
		log.Printf("Adding %s with json %s", k, postJson)
		err = AddToDedupeStore(tx, v)
		if err != nil {
			return err
		}
		return bucket.Put([]byte(k), postJson)
	})
}

func CheckIfMemeExists(tx *bolt.Tx, v memestream.Post) (bool, error) {
	bucket := tx.Bucket([]byte(dedupeBucket))
	imgURL, err := url.Parse(v.URL)
	if err != nil {
		return false, err
	}
	filepath := filePathFromURL(imgURL)
	if bucket.Get(filepath) != nil {
		return true, nil
	}
	return false, nil
}

func AddToDedupeStore(tx *bolt.Tx, v memestream.Post) error {
	bucket := tx.Bucket([]byte(dedupeBucket))
	imgURL, err := url.Parse(v.URL)
	if err != nil {
		return err
	}
	err = bucket.Put(filePathFromURL(imgURL), []byte{1})
	if err != nil {
		return err
	}
	return nil
}

func filePathFromURL(url *url.URL) []byte {
	return []byte(fmt.Sprintf("%s://%s%s", url.Scheme, url.Host, url.Path))
}
func (b *BoltDB) AsMap() (memes map[string]memestream.Post, err error) {
	memes = make(map[string]memestream.Post)
	err = b.conn.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(postBucket))
		return bucket.ForEach(func(k, v []byte) error {
			var post memestream.Post
			err := json.Unmarshal(v, &post)
			if err != nil {
				return err
			}
			memes[string(k)] = post
			return nil
		})
	})
	return
}

func (b *BoltDB) Close() error {
	return b.conn.Close()
}

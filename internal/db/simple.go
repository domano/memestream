package db

import "sync"

type SimplePosts struct {
	postMap map[string]string
	sync.RWMutex
}

func NewSimplePosts() *SimplePosts {
	postMap := make(map[string]string)
	return &SimplePosts{postMap:postMap}
}
func (p *SimplePosts) Get(k string) (v string, ok bool, err error) {
	p.RLock()
	defer p.RUnlock()
	v, ok = p.postMap[k]
	return
}

func (p *SimplePosts) Add(k, v string) error{
	p.Lock()
	defer p.Unlock()
	p.postMap[k] = v
	return nil
}

func (p *SimplePosts) AsMap() (postMap map[string]string, err error){
	return p.postMap, nil
}

func (p *SimplePosts) Close() error{
	return nil
}

package crawl


import (
	"github.com/gocolly/colly"
	"gitlab.com/domano/memestream"
	"log"
	"time"
)

func Reddit(adder Adder, subs ...string) {
	listCollector := colly.NewCollector()
	imageCollector := colly.NewCollector()
	imageCollector.Async = true

	scrapeTime := time.Now()

	// Find and visit all links
	listCollector.OnHTML("a[href*='comments']", func(e *colly.HTMLElement) {
		imageCollector.Visit(e.Attr("href"))
	})

	imageCollector.OnHTML(".media-element", func(e *colly.HTMLElement) {
		src := e.Attr("src")
		post := memestream.Post{
			URL: src,
			Created: scrapeTime,
		}
		err := adder.Add(e.Request.URL.String(), post)
		if err != nil {
			log.Fatal(err)
		}
	})

	for _, sub := range subs {
		listCollector.Visit(sub)
	}
	imageCollector.Wait()
}

type Adder interface {
	Add(string, memestream.Post) error
}


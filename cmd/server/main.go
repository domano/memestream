package main

import (
	"gitlab.com/domano/memestream"
	"gitlab.com/domano/memestream/internal/crawl"
	"gitlab.com/domano/memestream/internal/db"
	"gitlab.com/domano/memestream/internal/server"
	"log"
	"net/http"
	"time"
)

func main() {
	var posts Posts
	posts = db.NewBolt()
	defer posts.Close()

	renderer := server.NewTemplateRenderer(posts)

	log.Println("Starting crawlers...")
	go redditWorker(posts)

	log.Println("Starting server...")
	if err := http.ListenAndServe(":8080", renderer); err != nil {
		log.Fatal(err)
	}

}

func redditWorker(adder Adder) {
	crawlReddit(adder)
	ticker := time.Tick(1 * time.Minute)
	for {
		select {
		case <-ticker:
			crawlReddit(adder)
		}
	}
}

func crawlReddit(adder Adder) {
	log.Println("Crawling reddit...")
	crawl.Reddit(adder, "http://reddit.com/r/memes",
		"http://reddit.com/r/dankmemes",
		"http://reddit.com/r/lithuaniakittens",
		"http://reddit.com/r/ich_iel")
	log.Println("Done crawling reddit.")
}

type Posts interface {
	Adder
	AsMap() (map[string]memestream.Post, error)
	Close() error
}

type Adder interface {
	Add(k string, v memestream.Post) error
}

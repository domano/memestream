package memestream

import "time"

type Post struct {
	URL string
	Created time.Time
}
